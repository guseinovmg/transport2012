
import javax.microedition.lcdui.*;
import java.io.*;

public class Result extends List  {

    public Command backCommand,watchCommand;
    public Image busik;
    public Image autobus;
    public Image trolleybus;

    public Result(String title, int listType) {
        super(title, listType);
        backCommand = new Command(Strs.BACK, Command.ITEM, 1);
        watchCommand = new Command(Strs.WATCHING, Command.ITEM,2);
        this.addCommand(backCommand);
        this.addCommand(watchCommand);
        try {
            busik = Image.createImage("/busik.png");
            autobus = Image.createImage("/autobus.png");
            trolleybus = Image.createImage("/trolleybus.png");
        } catch (IOException ioe) {
        }
        this.setCommandListener(TRANSPORT2012.midlet);
    }

}
