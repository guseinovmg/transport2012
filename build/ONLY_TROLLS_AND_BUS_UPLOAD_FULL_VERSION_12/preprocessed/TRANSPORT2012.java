
import javax.microedition.io.Connector;
import javax.microedition.midlet.MIDlet;
import javax.microedition.lcdui.*;
import javax.microedition.rms.*;
import javax.wireless.messaging.MessageConnection;
import javax.wireless.messaging.TextMessage;

public class TRANSPORT2012 extends MIDlet implements CommandListener {

    public static Logo logo;
    public static SelectRoute map;
    public static Result result;
    public static Display display;
    public static Comp comp;
    public static ShowRoute showRoute;
    public static HelpForm helpForm;
    public static AskToActivateForm askToActivateForm;
    public static TRANSPORT2012 midlet;
    public static final int LOGO_SHOW_TIME_MILLIS = 1500;
    public static boolean midletPaused = false;
    public static boolean activated = false;
    public static String PARTNER_CODE = "12";
    public static final String SHORT_PHONE_NUMBER = "0550640768";

    public void startApp() {
        midlet = this;
        display = Display.getDisplay(this);
        if (midletPaused) {
        } else {
            logo = new Logo();
            display.setCurrent(logo);

            new Thread(new Runnable() {

                public void run() {
                    try {
                        Thread.sleep(LOGO_SHOW_TIME_MILLIS);
                    } catch (Exception ex) {
                    } finally {
                        map = new SelectRoute();
                        comp = new Comp();
                        showRoute = new ShowRoute();
                        helpForm = new HelpForm();
                        askToActivateForm = new AskToActivateForm();
                        result = new Result(Strs.FOUNDED, List.IMPLICIT);
                        display.setCurrent(map);
                        //#if RESTRICT_TYPE == "NONE"
//#                         activated = true;
                        //#else
                        activated = isActivated(getActivatingKey());
                        //#endif
//                        System.out.println(" is activated " + isActivated(getActivatingKey()));
//                        saveActivatingKey(getActivatingKey());
//                        System.out.println(" is activated " + isActivated(getActivatingKey()));
                    }
                }
            }).start();
        }
        midletPaused = false;

    }

    public TRANSPORT2012() {
//#if PARTNER_CODE == "12"
    PARTNER_CODE = "12";
//#elif PARTNER_CODE == "15"
//#     PARTNER_CODE = "15";
//#elif PARTNER_CODE == "10"
//#     PARTNER_CODE = "10";
//#endif
    }

    public void pauseApp() {
        midletPaused = true;
    }

    public void destroyApp(boolean unconditional) {
        notifyDestroyed();
    }

    public void commandAction(Command c, Displayable d) {
        if (d == showRoute) {
            ShowRoute.img = null;
            TRANSPORT2012.display.setCurrent(TRANSPORT2012.result);
        }
        if (d == result) {
            if (c == result.backCommand) {
                result.deleteAll();
                TRANSPORT2012.display.setCurrent(TRANSPORT2012.map);
            }
            if ((c == Result.SELECT_COMMAND || c == result.watchCommand) && result.size() > 0) {
                TRANSPORT2012.display.setCurrent(TRANSPORT2012.showRoute);
            }
        }
        if (d == map) {
            if (c == map.help) {
                TRANSPORT2012.display.setCurrent(helpForm);
            }
            if (c == map.exit) {
                if (activated) {
                    destroyApp(true);
                } else {
                    TRANSPORT2012.display.setCurrent(TRANSPORT2012.askToActivateForm);
                }

            }
            if (c == map.select) {
                map.selectStartOrFinishOfRoute();
            }
        }
        if(d == askToActivateForm){
            if(c == askToActivateForm.no || c == askToActivateForm.exit){
                destroyApp(true);
            }
            if(c == askToActivateForm.yes){
                activateBySMS();
            }
        }
        if (d == helpForm) {
            TRANSPORT2012.display.setCurrent(map);
        }
    }

    public boolean isActivated(String key) {
        RecordStore rec = null;
        try {
            rec = RecordStore.openRecordStore("Setting", true);
            if (rec.getNumRecords() == 0) {
                return false;
            } else {
                byte[] buf = rec.getRecord(1);
                String str = new String(buf);
                long intValueStr = Long.parseLong(str);
                if (key.hashCode() == intValueStr) {
                    return true;
                }
            }
        } catch (Exception ex) {
        } finally {
            if (rec != null) {
                try {
                    rec.closeRecordStore();
                } catch (Exception ex) {
                }
            }
        }
        return false;
    }

    public boolean saveActivatingKey(String key) {
        RecordStore rec = null;
        activated = true;
        try {
            rec = RecordStore.openRecordStore("Setting", true);
            String hashStr = String.valueOf(key.hashCode());
            byte[] buf = hashStr.getBytes();
            if (rec.getNumRecords() == 0) {
                rec.addRecord(buf, 0, buf.length);
            } else {
                rec.setRecord(1, buf, 0, buf.length);
            }
            return true;
        } catch (Exception ex) {
        } finally {
            if (rec != null) {
                try {
                    rec.closeRecordStore();
                } catch (Exception ex) {
                }
            }
        }
        return false;
    }

    public String getActivatingKey() {
        String res = "" + System.getProperty("microedition.platform");
        if (map != null) {
            res += map.getHeight();
            res += map.getWidth();
            res += map.hasPointerEvents();
            res += map.hasRepeatEvents();
        }
        System.out.println(" key  " + res);
        return res;
    }

    public void activateBySMS() {
        MessageConnection conn = null;
        String url = "sms://"+SHORT_PHONE_NUMBER;
        try {
            conn = (MessageConnection) Connector.open(url);
            TextMessage msg = (TextMessage) conn.newMessage(conn.TEXT_MESSAGE);
            msg.setPayloadText(PARTNER_CODE);
            this.saveActivatingKey(this.getActivatingKey());
            Alert alert = new Alert("!",Strs.ACTIVATION_SUCCESS,null,AlertType.INFO);
            display.setCurrent(alert, map);
        } catch (SecurityException ex) {
            Alert alert = new Alert("!",Strs.CANCEL_SMS_SENDING,null,AlertType.INFO);
            display.setCurrent(alert);
            destroyApp(true);
        } catch (Exception ex) {
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception ex) {
                }
            }
        }
    }
}
