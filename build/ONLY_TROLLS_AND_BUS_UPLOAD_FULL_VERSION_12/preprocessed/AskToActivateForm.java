
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Form;

public class AskToActivateForm extends Form {

    Command yes = new Command(Strs.YES, Command.ITEM, 0);
    Command no = new Command(Strs.NO, Command.ITEM, 0);
    Command exit = new Command(Strs.QUIT, Command.EXIT, 0);

    public AskToActivateForm() {
        super("!");
        this.append(Strs.ASK_FOR_ACTIVATION);
        //#if ACTIVATION_TYPE == "SEND_SMS_BY_PHONE"
//#         this.addCommand(yes);
//#         this.addCommand(no);
        //#elif ACTIVATION_TYPE == "UPLOAD_FULL_VERSION"
        this.addCommand(exit);
        //#endif
        this.setCommandListener(TRANSPORT2012.midlet);
    }
}
