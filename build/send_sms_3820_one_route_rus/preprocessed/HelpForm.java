
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Form;

public class HelpForm extends Form {
public Command back;
    public HelpForm() {
        super(Strs.HELP);
        back = new Command(Strs.BACK,Command.BACK,1);
        this.addCommand(back);
        this.setCommandListener(TRANSPORT2012.midlet);
        this.append(Strs.HELP_TEXT);
    }
}
