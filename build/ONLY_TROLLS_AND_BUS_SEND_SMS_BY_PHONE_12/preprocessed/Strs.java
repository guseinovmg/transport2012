
public class Strs {

    public static final String YES;
    public static final String NO;
    public static final String BACK;
    public static final String HELP;
    public static final String QUIT;
    public static final String SELECT;
    public static final String SHOW;
    public static final String WATCHING;
    public static final String HELP_TEXT;
    public static final String TRANSPORT;
    public static final String FOUNDED;
    public static final String OTKUDA;
    public static final String KUDA;
    public static final String ROUTES_NOT_NOT_FOUND;
    public static final String CANCEL_SMS_SENDING;
    public static final String ACTIVATION_SUCCESS;
    public static final String ASK_FOR_ACTIVATION;

    static {
        YES = "Да";
        NO = "Нет";
        BACK = "Назад";
        HELP = "Справка";
        QUIT = "Выход";
        SELECT = "Выбрать";
        SHOW = "Показать";
        WATCHING = "Просмотр";
        HELP_TEXT = " Справочник содержит все маршруты автобусов, троллейбусов и маршрутных такси г. Бишкек на 2012г. \n   Выберете точки на карте откуда и куда Вам нужно проехать, затем Вы можете просмотреть найденные маршруты на карте. Перемещаться на карте можно нажимая на клавиши стрелки или на цифры, или перемещая карту пальцем, если у телефона сенсорный дисплей. \n © Гусейнов М.Г. 2008-2012.\n guseinovmg@gmail.com ";
        TRANSPORT = "ТРАНСПОРТ";
        FOUNDED = "Найденные";
        OTKUDA = "ОТКУДА";
        KUDA = "КУДА";
        CANCEL_SMS_SENDING = "Отправление SMS отменено";
        ACTIVATION_SUCCESS = " Поздравляем! \n Справочник маршрутов теперь будет работать без ограничений";
        ROUTES_NOT_NOT_FOUND = " Маршруты по этому направлению не найдены";

        //#if RESTRICT_TYPE == "ONE_ROUTE"
        //#if ACTIVATION_TYPE == "SEND_SMS_BY_PHONE"
//#         ASK_FOR_ACTIVATION = "Это ограниченная версия, только первый найденный маршрут показан, нажмите Да чтобы отправить SMS ";
        //#elif ACTIVATION_TYPE == "UPLOAD_FULL_VERSION"
//#         ASK_FOR_ACTIVATION = "Это ограниченная версия, только первый найденный маршрут показан, отправте SMS на номер " + TRANSPORT2012.SHORT_PHONE_NUMBER + " с кодом " + TRANSPORT2012.PARTNER_CODE;
        //#endif
        //#elif RESTRICT_TYPE == "ONLY_TROLLS_AND_BUS"
        //#if ACTIVATION_TYPE == "SEND_SMS_BY_PHONE"
        ASK_FOR_ACTIVATION = "Это ограниченная версия,  только автобусы и троллейбусы найти можно, нажмите Да чтобы отправить SMS ";;
        //#elif ACTIVATION_TYPE == "UPLOAD_FULL_VERSION"
//#         ASK_FOR_ACTIVATION = "Это ограниченная версия, только автобусы и троллейбусы найти можно, отправте SMS на номер " + TRANSPORT2012.SHORT_PHONE_NUMBER + " с кодом " + TRANSPORT2012.PARTNER_CODE;
        //#endif
        //#elif RESTRICT_TYPE == "NONE"
//#         ASK_FOR_ACTIVATION = "";
        //#endif

    }
}
