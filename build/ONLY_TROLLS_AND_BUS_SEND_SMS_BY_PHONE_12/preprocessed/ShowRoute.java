
import javax.microedition.lcdui.*;
import java.io.*;

public final class ShowRoute extends Map {

    private static final int TARGET_PATH_COLOR = 0x00f07030;
    public static Image[][] img = null;
    private static int dx, dy, targetX, targetY, rows, cols,
            startCol, startRow,
            clipWidth, clipHeight,
            routeDx, routeDy;
    public static int startTargetX, startTargetY,
            finishTargetX, finishTargetY;
    private static short[] masi;
    private Command back = new Command(Strs.BACK, Command.ITEM, 1);

    public ShowRoute() {
        super();
        this.addCommand(back);
        this.setCommandListener(TRANSPORT2012.midlet);
//        this.setFullScreenMode(true);
    }

    public void paint(Graphics g) {
        int i, j;
        if (img == null) {
            clipWidth = g.getClipWidth();
            clipHeight = g.getClipHeight();
            targetX = SelectRoute.finishTargetX - SelectRoute.targDx;
            targetY = SelectRoute.finishTargetY - SelectRoute.targDy;
            if (targetX > 450) {
                targetX -= IMGSIZE;
            }
            if (targetY > 330) {
                targetY -= IMGSIZE;
            }
            rows = clipHeight / IMGSIZE + 2;
            cols = clipWidth / IMGSIZE + 2;
            startRow = (targetY - clipHeight / 2) / IMGSIZE;
            startCol = (targetX - clipWidth / 2) / IMGSIZE;
            img = new Image[cols][];
            for (i = 0; i < cols; i++) {
                img[i] = new Image[rows];
            }
            for (i = 0; i < cols; i++) {
                for (j = 0; j < rows; j++) {
                    try {
                        img[i][j] = Image.createImage("imgs" + imgStrings[i + startCol][j + startRow]);
                    } catch (IOException ioe) {
                        System.out.println("no image " + imgStrings[i + startCol][j + startRow]);
                    } catch (ArrayIndexOutOfBoundsException ai) {
                        System.out.println("no image ArrayIndexOutOfBoundsException ai");
                    }

                }
            }
            dx = (targetX - clipWidth / 2) - IMGSIZE * startCol;
            dy = (targetY - clipHeight / 2) - IMGSIZE * startRow;
        }

        for (i = 0; i < cols; i++) {
            for (j = 0; j < rows; j++) {
                try {
                    g.drawImage(img[i][j], i * IMGSIZE - dx, j * IMGSIZE - dy, Graphics.TOP | Graphics.LEFT);
                } catch (NullPointerException pe) {
                    System.out.println("no image " + i + " " + j);
                }
            }
        }


        routeDx = targetX - clipWidth / 2;
        routeDy = targetY - clipHeight / 2;

//        drawBoldDottedLine(g, startTargetX - routeDx, startTargetY - routeDy, finishTargetX - routeDx, finishTargetY - routeDy, 0x00a03060);
        fillLineI(g, startTargetX - routeDx, startTargetY - routeDy, finishTargetX - routeDx, finishTargetY - routeDy, 4, TARGET_PATH_COLOR);
        drawTarget(g, startTargetX - routeDx, startTargetY - routeDy, TARGET_PATH_COLOR);
        drawTarget(g, finishTargetX - routeDx, finishTargetY - routeDy, TARGET_PATH_COLOR);
        masi = Comp.result[TRANSPORT2012.result.getSelectedIndex()];
        g.setColor(0x00000080);
//System.out.println("Comp.checkSum =="+Comp.checkSum);
//        if (Comp.checkSum == 7974) {
        for (i = 1; i < masi.length - 3; i += 2) {
            try {
                int lineX1 = masi[i] - routeDx;
                int lineY1 = masi[i + 1] - routeDy;
                int lineX2 = masi[i + 2] - routeDx;
                int lineY2 = masi[i + 3] - routeDy;
                if ((lineX1 < 0 && lineX2 < 0) || (lineX1 > clipWidth && lineX2 > clipWidth)
                        || (lineY1 < 0 && lineY2 < 0) || (lineY1 > clipHeight && lineY2 > clipHeight)) {
//                    System.out.println(" line not painted ");
                    continue;
                }
                fillLineI(g, lineX1, lineY1, lineX2, lineY2, 4, 0x00007700);
            } catch (ArrayIndexOutOfBoundsException ai) {
                System.out.println("v nature " + (i + 3) + "bolshe " + (masi.length - 3));
            }
//            }
        }
        drawLabel(g, Strs.OTKUDA, startTargetX - routeDx - 12, startTargetY - routeDy + 10, 0x0000dd, 0x00ffffff);
        drawLabel(g, Strs.KUDA, finishTargetX - routeDx - 12, finishTargetY - routeDy + 10, 0x0000dd, 0x00ffffff);
        String busNumber = "№ " + TRANSPORT2012.result.getString(TRANSPORT2012.result.getSelectedIndex()).trim();
        drawLabel(g, busNumber, 0, 0, 0x0000c060, 0x00ffffff);

    }

    public void moveUp(final int STEP) {
        int i, j;
        dy -= STEP;
        targetY -= STEP;
        if (targetY - clipHeight / 2 < (startRow) * IMGSIZE) {
            if (startRow == 0) {
                dy += STEP;
                targetY += STEP;

            } else {
                for (i = 0; i < cols; i++) {
                    for (j = rows - 1; j > 0; j--) {
                        img[i][j] = img[i][j - 1];
                    }
                    try {
                        img[i][0] = Image.createImage("imgs" + imgStrings[startCol + i][startRow - 1]);
                    } catch (IOException ioe) {
                    }
                }
                startRow--;
                dy += IMGSIZE;
                System.gc();
            }
        }
    }

    public void moveDown(final int STEP) {
        int i, j;
        dy += STEP;
        targetY += STEP;
        if (targetY + clipHeight / 2 > (startRow + rows) * IMGSIZE) {
            if (startRow + rows == imgStrings[0].length) {
                dy -= STEP;
                targetY -= STEP;

            } else {
                for (i = 0; i < cols; i++) {
                    for (j = 0; j < rows - 1; j++) {
                        img[i][j] = img[i][j + 1];
                    }
                    try {
                        img[i][rows - 1] = Image.createImage("imgs" + imgStrings[startCol + i][startRow + rows]);
                    } catch (IOException ioe) {
                    }
                }
                startRow++;
                dy -= IMGSIZE;
                System.gc();
            }
        }
    }

    public void moveLeft(final int STEP) {
        int i, j;
        dx -= STEP;
        targetX -= STEP;
        if (targetX - clipWidth / 2 < (startCol) * IMGSIZE) {
            if (startCol == 0) {
                dx += STEP;
                targetX += STEP;

            } else {
                for (i = 0; i < rows; i++) {
                    for (j = cols - 1; j > 0; j--) {
                        img[j][i] = img[j - 1][i];
                    }
                    try {
                        img[0][i] = Image.createImage("imgs" + imgStrings[startCol - 1][startRow + i]);
                    } catch (IOException ioe) {
                    }
                }
                startCol--;
                dx += IMGSIZE;
                System.gc();
            }
        }
    }

    public void moveRight(final int STEP) {
        int i, j;
        dx += STEP;
        targetX += STEP;
        if (targetX + clipWidth / 2 > (startCol + cols) * IMGSIZE) {
            if (startCol + cols == imgStrings.length) {
                dx -= STEP;
                targetX -= STEP;

            } else {
                for (i = 0; i < rows; i++) {
                    for (j = 0; j < cols - 1; j++) {
                        img[j][i] = img[j + 1][i];
                    }
                    try {
                        img[cols - 1][i] = Image.createImage("imgs" + imgStrings[cols + startCol][i + startRow]);
                    } catch (IOException ioe) {
                    }
                }
                startCol++;
                dx -= IMGSIZE;
                System.gc();
            }
        }
    }

    public void keyPressed(int keyCode) {
        if (getGameAction(keyCode) == FIRE || keyCode == KEY_NUM5) {
        } else {
            keyRepeat(keyCode);
            startMoving(keyCode);
        }
    }

    public void keyReleased(int keyCode) {
        stopMoving();
    }
}
