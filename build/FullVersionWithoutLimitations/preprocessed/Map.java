
import javax.microedition.lcdui.*;

public abstract class Map extends Canvas {

    public static final int STEP = 8;
    static final int IMGSIZE = 48;
    static final int MOVING_DELAY_MILLIS = 70;
//    static final int DOTTED_LINE_STEP = 3;
//    static final int DOTTED_LINE_WIDTH = 5;
    boolean isMoving = false;
    CommandOnCanvas[] touchCommands;
    int startPointX, startPointY, finPointX, finPointY;
    public final static String[][] imgStrings = {{"/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/8.png", "/9.png", "/10.png", "/11.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/28.png", "/29.png", "/30.png", "/31.png", "/32.png", "/33.png", "/34.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/49.png", "/50.png", "/51.png", "/52.png", "/53.png", "/54.png", "/55.png", "/56.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/71.png", "/72.png", "/73.png", "/74.png", "/75.png", "/76.png", "/77.png", "/78.png", "/79.png", "/80.png", "/81.png", "/82.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/0.png", "/91.png", "/92.png", "/93.png", "/94.png", "/95.png", "/96.png", "/97.png", "/98.png", "/99.png", "/100.png", "/101.png", "/102.png", "/103.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/0.png", "/0.png", "/114.png", "/115.png", "/116.png", "/117.png", "/118.png", "/119.png", "/120.png", "/121.png", "/122.png", "/123.png", "/124.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/134.png", "/135.png", "/136.png", "/137.png", "/138.png", "/139.png", "/140.png", "/141.png", "/142.png", "/143.png", "/144.png", "/145.png", "/146.png", "/147.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/156.png", "/157.png", "/158.png", "/159.png", "/160.png", "/161.png", "/162.png", "/163.png", "/164.png", "/165.png", "/166.png", "/167.png", "/168.png", "/169.png", "/170.png", "/171.png", "/172.png", "/173.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/0.png", "/179.png", "/180.png", "/181.png", "/182.png", "/183.png", "/184.png", "/185.png", "/186.png", "/187.png", "/188.png", "/189.png", "/190.png", "/191.png", "/192.png", "/193.png", "/194.png", "/195.png", "/196.png", "/197.png"},
        {"/198.png", "/199.png", "/200.png", "/0.png", "/202.png", "/203.png", "/204.png", "/205.png", "/206.png", "/207.png", "/208.png", "/209.png", "/210.png", "/211.png", "/212.png", "/213.png", "/0.png", "/0.png", "/0.png", "/217.png", "/218.png", "/219.png"},
        {"/220.png", "/221.png", "/222.png", "/223.png", "/224.png", "/225.png", "/226.png", "/227.png", "/228.png", "/229.png", "/230.png", "/231.png", "/232.png", "/233.png", "/234.png", "/235.png", "/236.png", "/237.png", "/238.png", "/239.png", "/240.png", "/241.png"},
        {"/242.png", "/243.png", "/244.png", "/245.png", "/246.png", "/247.png", "/248.png", "/249.png", "/250.png", "/251.png", "/252.png", "/253.png", "/254.png", "/255.png", "/256.png", "/257.png", "/258.png", "/259.png", "/260.png", "/261.png", "/262.png", "/263.png"},
        {"/0.png", "/0.png", "/266.png", "/267.png", "/268.png", "/269.png", "/270.png", "/271.png", "/272.png", "/273.png", "/274.png", "/275.png", "/276.png", "/277.png", "/278.png", "/279.png", "/280.png", "/281.png", "/282.png", "/283.png", "/284.png", "/285.png"},
        {"/0.png", "/0.png", "/288.png", "/289.png", "/290.png", "/291.png", "/292.png", "/293.png", "/294.png", "/295.png", "/296.png", "/297.png", "/298.png", "/299.png", "/300.png", "/301.png", "/302.png", "/303.png", "/304.png", "/305.png", "/306.png", "/285.png"},
        {"/308.png", "/309.png", "/310.png", "/311.png", "/312.png", "/313.png", "/314.png", "/315.png", "/316.png", "/317.png", "/318.png", "/319.png", "/320.png", "/321.png", "/322.png", "/323.png", "/324.png", "/325.png", "/326.png", "/327.png", "/328.png", "/285.png"},
        {"/0.png", "/0.png", "/0.png", "/333.png", "/334.png", "/335.png", "/336.png", "/337.png", "/338.png", "/339.png", "/340.png", "/341.png", "/342.png", "/343.png", "/344.png", "/345.png", "/346.png", "/347.png", "/348.png", "/349.png", "/350.png", "/285.png"},
        {"/352.png", "/353.png", "/354.png", "/355.png", "/356.png", "/357.png", "/358.png", "/359.png", "/360.png", "/361.png", "/362.png", "/363.png", "/364.png", "/365.png", "/366.png", "/367.png", "/368.png", "/369.png", "/370.png", "/371.png", "/372.png", "/373.png"},
        {"/374.png", "/375.png", "/376.png", "/377.png", "/378.png", "/379.png", "/380.png", "/381.png", "/382.png", "/383.png", "/384.png", "/385.png", "/386.png", "/387.png", "/388.png", "/389.png", "/390.png", "/391.png", "/392.png", "/393.png", "/394.png", "/395.png"},
        {"/396.png", "/397.png", "/398.png", "/399.png", "/400.png", "/401.png", "/402.png", "/403.png", "/404.png", "/405.png", "/406.png", "/407.png", "/408.png", "/409.png", "/410.png", "/411.png", "/412.png", "/413.png", "/414.png", "/415.png", "/416.png", "/417.png"},
        {"/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/424.png", "/425.png", "/426.png", "/427.png", "/428.png", "/429.png", "/430.png", "/431.png", "/432.png", "/433.png", "/434.png", "/0.png", "/0.png", "/437.png", "/438.png", "/439.png"},
        {"/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/447.png", "/448.png", "/449.png", "/450.png", "/451.png", "/452.png", "/453.png", "/454.png", "/455.png", "/456.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/469.png", "/470.png", "/471.png", "/472.png", "/473.png", "/474.png", "/285.png", "/476.png", "/477.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/491.png", "/492.png", "/493.png", "/494.png", "/495.png", "/496.png", "/497.png", "/498.png", "/499.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/513.png", "/514.png", "/515.png", "/516.png", "/517.png", "/518.png", "/519.png", "/520.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/535.png", "/536.png", "/537.png", "/538.png", "/539.png", "/540.png", "/541.png", "/542.png", "/543.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/558.png", "/559.png", "/560.png", "/561.png", "/562.png", "/563.png", "/564.png", "/565.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/580.png", "/581.png", "/582.png", "/583.png", "/584.png", "/585.png", "/586.png", "/587.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"},
        {"/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/603.png", "/604.png", "/605.png", "/606.png", "/607.png", "/608.png", "/609.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png", "/0.png"}};
//    public abstract void keyRepeat(int keyCode);

    public static void drawLabel(Graphics g, String text, int x, int y, int textColor, int backColor) {
        int currentColor = g.getColor();
        Font font = g.getFont();
        int fontHeght = font.getHeight() + 4;
        int strLen = font.stringWidth(text) + 4;
        g.setColor(backColor);
        g.fillRect(x, y, strLen, fontHeght);
        g.setColor(textColor);
        g.drawString(text, x + 2, y + 2, Graphics.TOP | Graphics.LEFT);
        g.drawRect(x, y, strLen, fontHeght);
        g.setColor(currentColor);
    }

    public static void drawTarget(Graphics g, int x, int y, int color) {
        int currentColor = g.getColor();
        g.setColor(color);
        g.setColor(0x00ffffff);
        g.fillArc(x - 4, y - 4, 8, 8, 0, 360);
        g.setColor(color);
        g.fillRect(x - 11, y - 1, 8, 3);
        g.fillRect(x + 3, y - 1, 8, 3);
        g.fillRect(x - 1, y - 11, 3, 8);
        g.fillRect(x - 1, y + 3, 3, 8);
        g.setColor(currentColor);
    }

//    public static void drawBoldDottedLine(Graphics g, int x1, int y1, int x2, int y2, int color) {
//        int currentColor = g.getColor();
//        g.setColor(color);
//        int dx = x2 - x1;
//        int dy = y2 - y1;
//        int distance = sqrt(dx * dx + dy * dy);
//        int stepsCount = distance / DOTTED_LINE_STEP + ((distance % DOTTED_LINE_STEP > DOTTED_LINE_STEP / 2) ? 1 : 0);
//        int xStep = dx / stepsCount + ((dx % stepsCount > stepsCount / 2) ? 1 : 0);
//        int yStep = dy / stepsCount + ((dy % stepsCount > stepsCount / 2) ? 1 : 0);
//        for (int i = 0; i < stepsCount; i++) {
//            g.fillArc(x1 + xStep * i, y1 + yStep * i, DOTTED_LINE_WIDTH, DOTTED_LINE_WIDTH, 0, 360);
//        }
//        g.setColor(currentColor);
//    }

    //Процедура рисования линни заданной толщины, со встроенной функцией sqrt()
    public void fillLineI(Graphics g, int x1, int y1, int x2, int y2, int w, int color) {
        int currentColor = g.getColor();
        g.setColor(color);
        if (w == 1) {
            g.drawLine(x1, y1, x2, y2);
        } else {
            if (y1 == y2) {
                if (x1 > x2) {
                    g.fillRect(x2, y1 - w / 2, 1 + x1 - x2, w);
                } else {
                    g.fillRect(x1, y2 - w / 2, 1 + x2 - x1, w);
                }
                return;
            }
            if (x1 == x2) {
                if (y1 < y2) {
                    g.fillRect(x1 - w / 2, y1, w, 1 + y2 - y1);
                } else {
                    g.fillRect(x1 - w / 2, y2, w, 1 + y1 - y2);
                }
                return;
            }
            int a, b, l, lx1, lx2, lx3, lx4, ly1, ly2, ly3, ly4;
            a = (x2 - x1);
            b = (y2 - y1);
            l = 100 * (b * b + a * a);
            lx1 = l;
            lx2 = l;
            if (l > 0 && w > 0) {
                while (true) {
                    lx1 = (lx2 / lx1 + lx1) / 2;
                    if (l > lx1) {
                        l = lx1;
                    } else {
                        break;
                    }
                }
                lx1 = (5 + b * 100 * w / 2 / l + x1 * 10) / 10;
                ly1 = (5 + y1 * 10 - a * 100 * w / 2 / l) / 10;
                lx2 = (5 + b * 100 * w / 2 / l + x2 * 10) / 10;
                ly2 = (5 + y2 * 10 - a * 100 * w / 2 / l) / 10;
                lx3 = (5 + x1 * 10 - b * 100 * (w - (w / 2) - 1) / l) / 10;
                ly3 = (5 + a * 100 * (w - (w / 2) - 1) / l + y1 * 10) / 10;
                lx4 = (5 + x2 * 10 - b * 100 * (w - (w / 2) - 1) / l) / 10;
                ly4 = (5 + a * 100 * (w - (w / 2) - 1) / l + y2 * 10) / 10;
                g.fillTriangle(lx1, ly1, lx3, ly3, lx2, ly2);
                g.fillTriangle(lx4, ly4, lx3, ly3, lx2, ly2);
                if (Math.abs(a) > Math.abs(b)) {
                    g.drawLine(lx1, ly1, lx2, ly2);
                    g.drawLine(lx3, ly3, lx4, ly4);
                    g.drawLine(lx4, ly4, lx2, ly2);
                    g.drawLine(lx1, ly1, lx3, ly3);
                }
            }
        }
        g.setColor(currentColor);
    }

//    public static int sqrt(int l) {
//        int ret = l;
//        int div = l;
//        if (l <= 0) {
//            return 0;
//        }
//        while (true) {
//            div = (l / div + div) / 2;
//            if (ret > div) {
//                ret = div;
//            } else {
//                return ret;
//            }
//        }
//    }

    public void startMoving(final int keyCode) {
        isMoving = !isMoving;
        if (isMoving) {
            new Thread(new RunnableImpl(keyCode, this)).start();
        }
    }

    public void stopMoving() {
        isMoving = false;
    }

    abstract void moveLeft(final int STEP);

    abstract void moveRight(final int STEP);

    abstract void moveUp(final int STEP);

    abstract void moveDown(final int STEP);

    public void pointerPressed(int x, int y) {
        startPointX = x;
        startPointY = y;
    }

    public void pointerReleased(int x, int y) {
        int gDist = Math.abs(startPointX - x);
        int vDist = Math.abs(startPointY - y);

        int gSteps = gDist / STEP;
        int vSteps = vDist / STEP;

        if (startPointX > x) {
            for (int i = 0; i < gSteps; i++) {
                moveRight(STEP);
            }
            moveRight(gDist % STEP);
        } else {
            for (int i = 0; i < gSteps; i++) {
                moveLeft(STEP);
            }
            moveLeft(gDist % STEP);
        }
        if (startPointY > y) {
            for (int i = 0; i < vSteps; i++) {
                moveDown(STEP);
            }
            moveDown(vDist % STEP);
        } else {
            for (int i = 0; i < vSteps; i++) {
                moveUp(STEP);
            }
            moveUp(vDist % STEP);
        }
        if (this.touchCommands != null) {
            for (int i = 0; i < this.touchCommands.length; i++) {
                if (x == startPointX && y == startPointY && touchCommands[i].clicked(startPointX, startPointY)) {
                    touchCommands[i].click();
                }
            }
        }
        repaint();
    }

    public void pointerDragged(int x, int y) {
        pointerReleased(x, y);
        pointerPressed(x, y);
    }

    public void keyRepeat(int keyCode) {
        switch (keyCode) {
            case KEY_NUM1: {
                moveLeft(STEP);
                moveUp(STEP);
                repaint();
                break;
            }
            case KEY_NUM3: {
                moveRight(STEP);
                moveUp(STEP);
                repaint();
                break;
            }
            case KEY_NUM7: {
                moveLeft(STEP);
                moveDown(STEP);
                repaint();
                break;
            }
            case KEY_NUM9: {
                moveRight(STEP);
                moveDown(STEP);
                repaint();
                break;
            }
            case KEY_NUM4: {
                moveLeft(STEP);
                repaint();
                break;
            }
            case KEY_NUM2: {
                moveUp(STEP);
                repaint();
                break;
            }
            case KEY_NUM8: {
                moveDown(STEP);
                repaint();
                break;
            }
            case KEY_NUM6: {
                moveRight(STEP);
                repaint();
                break;
            }
            default: {
                keyCode = getGameAction(keyCode);
                switch (keyCode) {
                    case UP: {
                        moveUp(STEP);
                        repaint();
                        break;
                    }
                    case DOWN: {
                        moveDown(STEP);
                        repaint();
                        break;
                    }
                    case LEFT: {
                        moveLeft(STEP);
                        repaint();
                        break;
                    }
                    case RIGHT: {
                        moveRight(STEP);
                        repaint();
                        break;
                    }
                }
            }
        }
    }
}
