
import java.io.*;

public class Comp {

    private static final short[][] routes = new short[350][];
    public static int topRes, checkSum;
    public static final int RAD = 30;
    public static short[][] result = new short[RAD][];
    private InputStream is;
    private DataInputStream dis;

    public Comp() {
        is = getClass().getResourceAsStream("/bo");
        dis = new DataInputStream(is);
        short masilength = 0, i, j, number;
        for (i = 0; i < routes.length - 1; i++) {
            try {
                number = dis.readShort();
                masilength = dis.readShort();
            } catch (IOException ioe) {
                break;
            }
            routes[i] = new short[masilength += 3];
            routes[i][0] = number;
            checkSum += masilength;
            for (j = 1; j < masilength - 2; j++) {
                try {
                    routes[i][j] = dis.readShort();
                } catch (IOException ioe) {
                    System.out.println("nepravilniy file");
                    break;
                }
                routes[i][masilength - 1] = routes[i][masilength - 3];
                routes[i][masilength - 2] = routes[i][masilength - 4];
            }
        }
    }

    public final void findRoutes(int SX, int SY, int FX, int FY)//throws Exception
    {
//System.out.println("findRoutes");
        short number = 0;
        short[] masi;
        int i, j, x1, x2, y1, y2;
        topRes = 0;

        int sax = SX - RAD * 2;
        int sbx = SX - RAD;
        int scx = SX + RAD;
        int sdx = SX + RAD * 2;

        int say = SY - RAD * 2;
        int sby = SY - RAD;
        int scy = SY + RAD;
        int sdy = SY + RAD * 2;

        int fax = FX - RAD * 2;
        int fbx = FX - RAD;
        int fcx = FX + RAD;
        int fdx = FX + RAD * 2;

        int fay = FY - RAD * 2;
        int fby = FY - RAD;
        int fcy = FY + RAD;
        int fdy = FY + RAD * 2;

        for (i = 0; (i < routes.length) && (topRes < RAD - 1); i++) {
            try {
                masi = routes[i];
                number = masi[0];
                if ((masi == null) || (topRes > 0 && result[topRes - 1][0] == number)) {
                    continue;
                }
            } catch (NullPointerException pe) {
                continue;
            }

            for (j = 1; j < masi.length - 3; j += 2) {
                x1 = masi[j];
                y1 = masi[j + 1];
                x2 = masi[j + 2];
                y2 = masi[j + 3];
                if ((x1 < sdx && x1 > sax || y1 < sdy && y1 > say)
                        && ((x1 < scx && x1 > sbx && y1 < scy && y1 > sby)
                        || (x1 < scx && x1 > sax && y1 > scy && y2 < scy && x2 < sdx && x2 > sbx)
                        || (x1 < sbx && y1 > sby && y1 < sdy && x2 > sbx && y2 > say && y2 < scy)
                        || (x1 < sbx && y1 > say && y1 < scy && x2 > sbx && y2 > sby && y2 < sdy)
                        || (x1 < scx && x1 > sax && y1 < sby && y2 > sby && x2 > sbx && x2 < sdx)
                        || (x1 < sdx && x1 > sbx && y1 < sby && y2 > sby && x2 < scx && x2 > sax)
                        || (x1 > scx && y1 > say && y1 < scy && x2 < scx && y2 < sdy && y2 > sby)
                        || (x1 > scx && y1 > sby && y1 < sdy && x2 < scx && y2 > say && y2 < scy)
                        || (x1 > sbx && x1 < sdx && y1 > scy && x2 > sax && x2 < scx && y2 < scy))) {
                    for (j = number < 0 ? 1 : j; j < masi.length - 3; j += 2) {
                        x1 = masi[j];
                        y1 = masi[j + 1];
                        x2 = masi[j + 2];
                        y2 = masi[j + 3];
                        if ((x1 < fdx && x1 > fax || y1 < fdy && y1 > fay)
                                && ((x1 < fcx && x1 > fbx && y1 < fcy && y1 > fby)
                                || (x1 < fcx && x1 > fax && y1 > fcy && y2 < fcy && x2 < fdx && x2 > fbx)
                                || (x1 < fbx && y1 > fby && y1 < fdy && x2 > fbx && y2 > fay && y2 < fcy)
                                || (x1 < fbx && y1 > fay && y1 < fcy && x2 > fbx && y2 > fby && y2 < fdy)
                                || (x1 < fcx && x1 > fax && y1 < fby && y2 > fby && x2 > fbx && x2 < fdx)
                                || (x1 < fdx && x1 > fbx && y1 < fby && y2 > fby && x2 < fcx && x2 > fax)
                                || (x1 > fcx && y1 > fay && y1 < fcy && x2 < fcx && y2 < fdy && y2 > fby)
                                || (x1 > fcx && y1 > fby && y1 < fdy && x2 < fcx && y2 > fay && y2 < fcy)
                                || (x1 > fbx && x1 < fdx && y1 > fcy && x2 > fax && x2 < fcx && y2 < fcy))) {
                            result[topRes++] = masi;
                            if (number < -99 || number > 99) {
                                TRANSPORT2012.result.append("   " + (number < 0 ? -number : number), TRANSPORT2012.result.busik);
                            } else {
                                switch (number) {
                                    case 8:
                                    case 10:
                                    case -11:
                                    case -13:
                                    case -14:
                                    case 16:
                                    case -17: {
                                        TRANSPORT2012.result.append("   " + (number < 0 ? -number : number), TRANSPORT2012.result.trolleybus);
                                        break;
                                    }
                                    default: {
                                        TRANSPORT2012.result.append("   " + (number < 0 ? -number : number), TRANSPORT2012.result.autobus);
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }

        /*try{
        //System.out.println(System.currentTimeMillis());
        //System.out.println(RecordStore.openRecordStore("www",true).getLastModified());
        //System.out.println(System.currentTimeMillis()-RecordStore.openRecordStore("www",true).getLastModified());
        if((System.currentTimeMillis()-RecordStore.openRecordStore("www",true).getLastModified()) > 300000L)
        TRANSPORT.result.append("vreme idet",null);
        else TRANSPORT.result.append("vreme vishel",null);
        TRANSPORT.result.append(System.currentTimeMillis()+"  ",null);
        TRANSPORT.result.append(RecordStore.openRecordStore("www",true).getLastModified()+"  ",null);
        // 15984000000L
        } catch (Exception rs) {TRANSPORT.result.append(rs.toString(),null); TRANSPORT.result.append("errora",null);}
         */
    }
}
