
import javax.microedition.midlet.MIDlet;
import javax.microedition.lcdui.*;

public class TRANSPORT2012 extends MIDlet implements CommandListener {

    public static Logo logo;
    public static SelectRoute map;
    public static Result result;
    public static Display display;
    public static Comp comp;
    public static ShowRoute showRoute;
    public static HelpForm helpForm;
    public static TRANSPORT2012 midlet;
    public static final int LOGO_SHOW_TIME_MILLIS = 1500;
    public static boolean midletPaused = false;

    public void startApp() {
        midlet = this;
        display = Display.getDisplay(this);
        if (midletPaused) {
        } else {
            logo = new Logo();
            display.setCurrent(logo);

            new Thread(new Runnable() {

                public void run() {
                    try {
                        Thread.sleep(LOGO_SHOW_TIME_MILLIS);
                    } catch (Exception ex) {
                    } finally {
                        map = new SelectRoute();
                        comp = new Comp();
                        showRoute = new ShowRoute();
                        helpForm = new HelpForm();
                        result = new Result(Strs.FOUNDED, List.IMPLICIT);
                        display.setCurrent(map);
                    }
                }
            }).start();
        }
        midletPaused = false;

//#ifdef RESTRICT_TYPE
        System.out.println("def!");
//#else
//#         System.out.println(" not def!");
//#endif

    }

    public TRANSPORT2012() {
    }

    public void pauseApp() {
        midletPaused = true;
    }

    public void destroyApp(boolean unconditional) {
        notifyDestroyed();
    }

    public void commandAction(Command c, Displayable d) {
        if (d == showRoute) {
            ShowRoute.img = null;
            TRANSPORT2012.display.setCurrent(TRANSPORT2012.result);
        }
        if (d == result) {
            if (c == result.backCommand) {
                result.deleteAll();
                TRANSPORT2012.display.setCurrent(TRANSPORT2012.map);
            }
            if ((c == Result.SELECT_COMMAND || c == result.watchCommand) && result.size() > 0) {
                TRANSPORT2012.display.setCurrent(TRANSPORT2012.showRoute);
            }
        }
        if (d == map) {
            if (c == map.help) {
                TRANSPORT2012.display.setCurrent(helpForm);
            }
            if (c == map.exit) {
                destroyApp(true);
            }
            if (c == map.select) {
                map.selectStartOrFinishOfRoute();
            }
        }
        if (d == helpForm) {
            TRANSPORT2012.display.setCurrent(map);
        }
    }
}
