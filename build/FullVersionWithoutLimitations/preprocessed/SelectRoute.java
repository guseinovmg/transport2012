
import javax.microedition.lcdui.*;
import java.io.*;
import javax.microedition.lcdui.Graphics;

public final class SelectRoute extends Map {

    private static final int START = 0;
    private static final int FINISH = 1;
    private static final int TARGET_PATH_COLOR = 0x003355ff;
    private static String stringToShow = Strs.OTKUDA;
    private static Image[][] img = null;
    public static int dx, dy, targetX, targetY, rows, cols,
            startCol, startRow,
            clipWidth, clipHeight,
            targDx, targDy,
            startTargetX, startTargetY,
            finishTargetX, finishTargetY, choice = START;
    Command help = new Command(Strs.HELP, Command.HELP, 1);
    Command exit = new Command(Strs.QUIT, Command.EXIT, 0);
    Command select = new Command(Strs.SELECT, Command.OK, 0);
    CommandOnCanvas selectTouch;

    public SelectRoute() {
        super();
        selectTouch = new CommandOnCanvas(select, this, Strs.SELECT, 0x00aa0055, 0x00ffffff);
        if(this.hasPointerEvents()){
          this.touchCommands = new CommandOnCanvas[]{selectTouch};  
        }
        this.addCommand(help);
        this.addCommand(exit);
        this.addCommand(select);
        this.setCommandListener(TRANSPORT2012.midlet);
    }

    public void paint(Graphics g) {
        int i, j;
        if (img == null) {
//System.out.println("hash "+Strs.HELP_TEXT.hashCode());
            targDx = 0;
            targDy = 0;
            clipWidth = g.getClipWidth();
            clipHeight = g.getClipHeight();
            targetX = imgStrings.length * IMGSIZE / 2;
            targetY = imgStrings[0].length * IMGSIZE / 2;
            rows = clipHeight / IMGSIZE + 2;
            cols = clipWidth / IMGSIZE + 2;
            startRow = (targetY - clipHeight / 2) / IMGSIZE;
            startCol = (targetX - clipWidth / 2) / IMGSIZE;
            if (Strs.HELP_TEXT.hashCode() == 1004981202) {
                img = new Image[cols][];
            }
            for (i = 0; i < cols; i++) {
                img[i] = new Image[rows];
            }
            for (i = 0; i < cols; i++) {
                for (j = 0; j < rows; j++) {
                    try {
                        img[i][j] = Image.createImage(imgStrings[i + startCol][j + startRow]);
                    } catch (IOException ioe) {
                        System.out.println("no image " + imgStrings[i + startCol][j + startRow]);
                    }
                }
            }
            dx = (targetX - clipWidth / 2) - IMGSIZE * startCol;
            dy = (targetY - clipHeight / 2) - IMGSIZE * startRow;
            Font font = g.getFont();
            this.selectTouch.setHeight(font.getHeight() + 4);
            this.selectTouch.setWidth(font.stringWidth(this.selectTouch.text) + 4);
            this.selectTouch.setX(clipWidth - this.selectTouch.getWidth());
            this.selectTouch.setY(clipHeight - this.selectTouch.getHeight());
        }
//        g.setFont(Font.getFont(Font.FACE_MONOSPACE, Font.STYLE_BOLD | Font.STYLE_ITALIC, Font.SIZE_MEDIUM));
        for (i = 0; i < cols; i++) {
            for (j = 0; j < rows; j++) {
                try {
                    g.drawImage(img[i][j], i * IMGSIZE - dx, j * IMGSIZE - dy, Graphics.TOP | Graphics.LEFT);
                } catch (NullPointerException pe) {
                    System.out.println("no image " + i + " " + j);
                }
            }
        }

        
        if (choice == FINISH) {
            int stX = startTargetX - targetX + clipWidth / 2;
            int stY = startTargetY - targetY + clipHeight / 2;
            fillLineI(g, stX, stY, clipWidth / 2 + targDx, clipHeight / 2 + targDy, 4, TARGET_PATH_COLOR);
            drawTarget(g, stX, stY, TARGET_PATH_COLOR);
        }

        drawTarget(g, clipWidth / 2 + targDx, clipHeight / 2 + targDy, TARGET_PATH_COLOR);
        drawLabel(g, stringToShow, clipWidth / 2 - 12 + targDx, clipHeight / 2 + 10 + targDy, TARGET_PATH_COLOR, 0x00ffffff);
        if (this.touchCommands != null) {
            for (i = 0; i < this.touchCommands.length; i++) {
                touchCommands[i].draw(g);
            }
        }
    }

    public void moveUp(final int STEP) {
        int i, j;
        if (targDy > 0) {
            targDy -= STEP;
        } else {
            dy -= STEP;
            targetY -= STEP;
            if (targetY - clipHeight / 2 < (startRow) * IMGSIZE) {
                if (startRow == 0) {
                    dy += STEP;
                    targetY += STEP;
                    if (targDy - 5 > -clipHeight / 2) {
                        targDy -= STEP;
                    }
                } else {
                    for (i = 0; i < cols; i++) {
                        for (j = rows - 1; j > 0; j--) {
                            img[i][j] = img[i][j - 1];
                        }
                        try {
                            img[i][0] = Image.createImage(imgStrings[startCol + i][startRow - 1]);
                        } catch (IOException ioe) {
                        }
                    }
                    startRow--;
                    dy += IMGSIZE;
                    System.gc();
                }
            }
        }
    }

    public void moveDown(final int STEP) {
        int i, j;
        if (targDy < 0) {
            targDy += STEP;
        } else {
            dy += STEP;
            targetY += STEP;
            if (targetY + clipHeight / 2 > (startRow + rows) * IMGSIZE) {
                if (startRow + rows == imgStrings[0].length) {
                    dy -= STEP;
                    targetY -= STEP;
                    if (targDy + 5 < clipHeight / 2) {
                        targDy += STEP;
                    }
                } else {
                    for (i = 0; i < cols; i++) {
                        for (j = 0; j < rows - 1; j++) {
                            img[i][j] = img[i][j + 1];
                        }
                        try {
                            img[i][rows - 1] = Image.createImage(imgStrings[startCol + i][startRow + rows]);
                        } catch (IOException ioe) {
                        }
                    }
                    startRow++;
                    dy -= IMGSIZE;
                    System.gc();
                }
            }
        }
    }

    public void moveLeft(final int STEP) {
        int i, j;
        if (targDx > 0) {
            targDx -= STEP;
        } else {
            dx -= STEP;
            targetX -= STEP;
            if (targetX - clipWidth / 2 < (startCol) * IMGSIZE) {
                if (startCol == 0) {
                    dx += STEP;
                    targetX += STEP;
                    if (targDx - 5 > -clipWidth / 2) {
                        targDx -= STEP;
                    }
                } else {
                    for (i = 0; i < rows; i++) {
                        for (j = cols - 1; j > 0; j--) {
                            img[j][i] = img[j - 1][i];
                        }
                        try {
                            img[0][i] = Image.createImage(imgStrings[startCol - 1][startRow + i]);
                        } catch (IOException ioe) {
                        }
                    }
                    startCol--;
                    dx += IMGSIZE;
                    System.gc();
                }
            }
        }
    }

    public void moveRight(final int STEP) {
        int i, j;
        if (targDx < 0) {
            targDx += STEP;
        } else {
            dx += STEP;
            targetX += STEP;
            if (targetX + clipWidth / 2 > (startCol + cols) * IMGSIZE) {
                if (startCol + cols == imgStrings.length) {
                    dx -= STEP;
                    targetX -= STEP;
                    if (targDx + 5 < clipWidth / 2) {
                        targDx += STEP;
                    }
                } else {
                    for (i = 0; i < rows; i++) {
                        for (j = 0; j < cols - 1; j++) {
                            img[j][i] = img[j + 1][i];
                        }
                        try {
                            img[cols - 1][i] = Image.createImage(imgStrings[cols + startCol][i + startRow]);
                        } catch (IOException ioe) {
                        }
                    }
                    startCol++;
                    dx -= IMGSIZE;
                    System.gc();
                }
            }
        }
    }

    public void keyPressed(int keyCode) {
        if ((getGameAction(keyCode) == FIRE || keyCode == KEY_NUM5)) {
            selectStartOrFinishOfRoute();
        } else {
            keyRepeat(keyCode);
            startMoving(keyCode);
        }
    }

    public void keyReleased(int keyCode) {
        stopMoving();
    }

    public void selectStartOrFinishOfRoute() {
        if (choice == START) {
            startTargetX = targetX + targDx;
            startTargetY = targetY + targDy;
            choice = FINISH;
            stringToShow = Strs.KUDA;
            repaint();

        } else {
            finishTargetX = targetX + targDx;
            finishTargetY = targetY + targDy;
            choice = START;
            stringToShow = Strs.OTKUDA;
            img = null;
            ShowRoute.startTargetX = startTargetX;
            ShowRoute.finishTargetY = finishTargetY;
            ShowRoute.startTargetY = startTargetY;
            ShowRoute.finishTargetX = finishTargetX;
            TRANSPORT2012.comp.findRoutes(startTargetX, startTargetY, finishTargetX, finishTargetY);
            if (TRANSPORT2012.result.size() == 0) {
                Alert alert = new Alert("", Strs.ROUTES_NOT_NOT_FOUND, null, AlertType.INFO);
                alert.setTimeout(3500);
                TRANSPORT2012.display.setCurrent(alert, TRANSPORT2012.result);
            } else {
                TRANSPORT2012.display.setCurrent(TRANSPORT2012.result);
            }
        }
    }
}
