
public class Strs {

    public static final String BACK;
    public static final String HELP;
    public static final String QUIT;
    public static final String SELECT;
    public static final String SHOW;
    public static final String WATCHING;
    public static final String HELP_TEXT;
    public static final String TRANSPORT;
    public static final String FOUNDED;
    public static final String OTKUDA;
    public static final String KUDA;
    public static final String ROUTES_NOT_NOT_FOUND;

    static {
        BACK = "Назад";
        HELP = "Справка";
        QUIT = "Выход";
        SELECT = "Выбрать";
        SHOW = "Показать";
        WATCHING = "Просмотр";
        HELP_TEXT = " Справочник содержит все маршруты автобусов, троллейбусов и маршрутных такси г. Бишкек на 2012г. \n   Выберете точки на карте откуда и куда Вам нужно проехать, затем Вы можете просмотреть найденные маршруты на карте. Перемещаться на карте можно нажимая на клавиши стрелки или на цифры, или перемещая карту пальцем, если у телефона сенсорный дисплей. \n © Гусейнов М.Г. 2008-2012.\n guseinovmg@gmail.com ";
        TRANSPORT = "ТРАНСПОРТ";
        FOUNDED = "Найденные";
        OTKUDA = "ОТКУДА";
        KUDA = "КУДА";
        ROUTES_NOT_NOT_FOUND =" Маршруты по этому направлению не найдены";
    }
}
