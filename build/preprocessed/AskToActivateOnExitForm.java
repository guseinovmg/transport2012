
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Form;

public class AskToActivateOnExitForm extends Form {

    Command send_SMS = new Command(Strs.SEND, Command.EXIT, 1);
    Command exit = new Command(Strs.QUIT, Command.EXIT, 2);

    public AskToActivateOnExitForm() {
        super("!");
        this.append(Strs.ASK_FOR_ACTIVATION_ON_RESULT);
        //#if ACTIVATION_TYPE == "SEND_SMS_BY_PHONE"
//#         this.addCommand(send_SMS);
        //#elif ACTIVATION_TYPE == "LOAD_FULL_VERSION"
//#         
        //#endif
        this.addCommand(exit);
        this.setCommandListener(TRANSPORT2012.midlet);
    }
}
