
class RunnableImpl implements Runnable {

    private final int keyCode;
    Map outer;

    public RunnableImpl(int keyCode, Map outer) {
        this.outer = outer;
        this.keyCode = keyCode;
    }

    public void run() {
        try {
            while (outer.isMoving) {
                Thread.sleep(Map.MOVING_DELAY_MILLIS);
                outer.keyRepeat(keyCode);
            }
        } catch (Exception ex) {
        } finally {
        }
    }
}
