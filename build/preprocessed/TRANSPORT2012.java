
import javax.microedition.io.Connector;
import javax.microedition.midlet.MIDlet;
import javax.microedition.lcdui.*;
import javax.microedition.rms.*;
import javax.wireless.messaging.MessageConnection;
import javax.wireless.messaging.TextMessage;

public class TRANSPORT2012 extends MIDlet implements CommandListener {

    public static Logo logo;
    public static SelectRoute map;
    public static Result result;
    public static Display display;
    public static Comp comp;
    public static ShowRoute showRoute;
    public static HelpForm helpForm;
    public static AskToActivateOnExitForm askToActivateFormOnExit;
    public static AskToActivateForm askToActivateForm;
    public static TRANSPORT2012 midlet;
    public static final int LOGO_SHOW_TIME_MILLIS = 1500;
    public static boolean midletPaused = false;
    public static boolean activated = false;
    public static Form waiteForm;
    public static String PARTNER_CODE = "tr2012";
    public static final String SHORT_PHONE_NUMBER = "8887";

    public void startApp() {
  
        midlet = this;
        display = Display.getDisplay(this);
        if (midletPaused) {
        } else {
            logo = new Logo();
            display.setCurrent(logo);

            new Thread(new Runnable() {

                public void run() {
                    long time = System.currentTimeMillis();
                    try {
                        Thread.sleep(200);
                    } catch (Exception ex) {
                    } finally {
                    }
                    map = new SelectRoute();
                    comp = new Comp();
                    showRoute = new ShowRoute();
                    helpForm = new HelpForm();
                    askToActivateFormOnExit = new AskToActivateOnExitForm();
                    result = new Result(Strs.FOUNDED, List.IMPLICIT);
                    askToActivateForm = new AskToActivateForm();
                    if (map.hasPointerEvents()) {
                        result.removeCommand(result.watchRouteCommand);
                    }

                    //#if RESTRICT_TYPE == "NONE"
                        activated = true;
                    //#else
//#                     activated = isActivated(getActivatingKey());
                    //#endif
                    long elapsedTime = System.currentTimeMillis() - time;
                    if (elapsedTime < LOGO_SHOW_TIME_MILLIS) {
                        try {
                            Thread.sleep(LOGO_SHOW_TIME_MILLIS - elapsedTime);
                        } catch (Exception ex) {
                        } finally {
                        }
                    }
//                    Alert alert = new Alert("", "time = " + (System.currentTimeMillis() - time + " elapsedTime " + elapsedTime), null, AlertType.ALARM);
//                    display.setCurrent(alert, map);
                    display.setCurrent(map);

                }
            }).start();
        }
        midletPaused = false;

    }

    public TRANSPORT2012() {
//#if PARTNER_CODE == "tr2012"
//#         PARTNER_CODE = "tr2012";
//#elif PARTNER_CODE == "15"
//#     PARTNER_CODE = "15";
//#elif PARTNER_CODE == "10"
//#     PARTNER_CODE = "10";
//#endif
    }

    public void pauseApp() {
        midletPaused = true;
    }

    public void destroyApp(boolean unconditional) {
        notifyDestroyed();
    }

    public void commandAction(Command c, Displayable d) {
        if (d == showRoute) {
            ShowRoute.img = null;
            if (activated) {
                display.setCurrent(result);
            } else {
                display.setCurrent(askToActivateForm);
            }
        }
        if (d == result) {
            if (c == result.searchCommand) {
                result.deleteAll();
                display.setCurrent(map);
            }
            if ((c == Result.SELECT_COMMAND || c == result.watchRouteCommand) && result.size() > 0) {
                display.setCurrent(showRoute);
            }
            if (c == result.exit) {
                destroyApp(true);
            }
        }
        if (d == map) {
            if (c == map.help) {
                display.setCurrent(helpForm);
            }
            if (c == map.exit) {
                destroyApp(true);
//                if (activated) {
//                    destroyApp(true);
//                } else {
//                    display.setCurrent(askToActivateFormOnExit);
//                }
            }
            if (c == map.select) {
                map.selectStartOrFinishOfRoute();
            }
        }
        if (d == askToActivateForm) {
            if (c == askToActivateForm.watchRouteCommand) {
                result.setSelectedIndex(0, true);
                display.setCurrent(showRoute);
            }
            if (c == askToActivateForm.activateBySMSCommand) {
                activateBySMS(result, askToActivateForm);
            }
            if (c == askToActivateForm.searchCommand) {
                result.deleteAll();
                display.setCurrent(map);
            }
        }
        if (d == askToActivateFormOnExit) {
            if (c == askToActivateFormOnExit.exit) {
                destroyApp(true);
            }
            if (c == askToActivateFormOnExit.send_SMS) {
                activateBySMS(map, askToActivateFormOnExit);
            }
        }
        if (d == helpForm) {
            display.setCurrent(map);
        }
    }

    public boolean isActivated(String key) {
        RecordStore rec = null;
        try {
            rec = RecordStore.openRecordStore("Setting", true);
            if (rec.getNumRecords() == 0) {
                return false;
            } else {
                byte[] buf = rec.getRecord(1);
                String str = new String(buf);
                long intValueStr = Long.parseLong(str);
                if (key.hashCode() == intValueStr) {
                    return true;
                }
            }
        } catch (Exception ex) {
        } finally {
            if (rec != null) {
                try {
                    rec.closeRecordStore();
                } catch (Exception ex) {
                }
            }
        }
        return false;
    }

    public boolean saveActivatingKey(String key) {
        RecordStore rec = null;
        activated = true;
        try {
            rec = RecordStore.openRecordStore("Setting", true);
            String hashStr = String.valueOf(key.hashCode());
            byte[] buf = hashStr.getBytes();
            if (rec.getNumRecords() == 0) {
                rec.addRecord(buf, 0, buf.length);
            } else {
                rec.setRecord(1, buf, 0, buf.length);
            }
            return true;
        } catch (Exception ex) {
        } finally {
            if (rec != null) {
                try {
                    rec.closeRecordStore();
                } catch (Exception ex) {
                }
            }
        }
        return false;
    }

    public String getActivatingKey() {
        String res = "null" + System.getProperty("microedition.platform") + this.getAppProperty(Strs.SERIAL);
        if (map != null) {
            res += map.getHeight();
            res += map.getWidth();
            res += map.hasPointerEvents();
            res += map.hasRepeatEvents();
        }
        System.out.println(" key  " + res);
        return res;
    }

    public void activateBySMS(final Displayable dispOnSucces, final Displayable dispIfNotSucces) {
        display.setCurrent(getWaiteForm());
        new Thread(new Runnable() {

            public void run() {
                MessageConnection conn = null;
                String url = "sms://" + SHORT_PHONE_NUMBER;
                try {
                    Thread.sleep(200);
                    conn = (MessageConnection) Connector.open(url);
                    TextMessage msg = (TextMessage) conn.newMessage(conn.TEXT_MESSAGE);
                    msg.setPayloadText(PARTNER_CODE + " " + TRANSPORT2012.midlet.getAppProperty(Strs.SERIAL));
                    conn.send(msg);
                    saveActivatingKey(getActivatingKey());
                    Alert alert = new Alert("!", Strs.ACTIVATION_SUCCESS, null, AlertType.INFO);
                    alert.setTimeout(3000);
                    display.setCurrent(alert, dispOnSucces);
                } catch (SecurityException ex) {
                    Alert alert = new Alert("!", Strs.CANCEL_SMS_SENDING, null, AlertType.INFO);
                    alert.setTimeout(2500);
                    display.setCurrent(alert, dispIfNotSucces);
                } catch (Exception ex) {
                    Alert alert = new Alert("!", Strs.SMS_ERROR_TRY_LATER, null, AlertType.INFO);
                    alert.setTimeout(Alert.FOREVER);
                    display.setCurrent(alert, dispIfNotSucces);
                } finally {
                    if (conn != null) {
                        try {
                            conn.close();
                        } catch (Exception ex) {
                        }
                    }
                }
            }
        }).start();

    }

    public static Form getWaiteForm() {
        if (waiteForm == null) {
            waiteForm = new Form("!");
            waiteForm.append(new Gauge(Strs.WAITE_SMS_IS_SENDING, false, Gauge.INDEFINITE, Gauge.CONTINUOUS_RUNNING));
        }
        return waiteForm;
    }
}
