
import java.io.IOException;
import javax.microedition.lcdui.*;

public abstract class Map extends Canvas {

    public static final int STEP = 8;
    static final int IMGSIZE = 48;
    static final int MOVING_DELAY_MILLIS = 70;
    boolean isMoving = false;
    String jarSize;
    CommandOnCanvas[] touchCommands;
    int startPointX, startPointY, finPointX, finPointY;
    final static String mapImagesFolder = "/imgs/";
    public final static int[][] mapImages = {{0, 0, 0, 0, 0, 0, 0, 0, 8, 9, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 28, 29, 30, 31, 32, 33, 34, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 49, 50, 51, 52, 53, 54, 55, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 0, 0, 0, 0, 0},
        {0, 0, 0, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 0, 0, 0, 0, 0, 0},
        {0, 0, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 0, 0},
        {0, 0, 0, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197},
        {198, 199, 200, 0, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 0, 0, 0, 217, 218, 219},
        {220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241},
        {242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263},
        {0, 0, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285},
        {0, 0, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 285},
        {308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 285},
        {0, 0, 0, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 285},
        {352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373},
        {374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395},
        {396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417},
        {0, 0, 0, 0, 0, 0, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 0, 0, 437, 438, 439},
        {0, 0, 0, 0, 0, 0, 0, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 469, 470, 471, 472, 473, 474, 285, 476, 477, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 491, 492, 493, 494, 495, 496, 497, 498, 499, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 513, 514, 515, 516, 517, 518, 519, 520, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 535, 536, 537, 538, 539, 540, 541, 542, 543, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 558, 559, 560, 561, 562, 563, 564, 565, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 580, 581, 582, 583, 584, 585, 586, 587, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 603, 604, 605, 606, 607, 608, 609, 0, 0, 0, 0, 0, 0}};
    public final static int[][] ShiftsOfPointer = {
        {0, 2, -15, -5, -15, 5},
        {2, 7, -15, -6, -15, 5},
        {7, 11, -15, -7, -15, 4},
        {11, 14, -14, -7, -16, 4},
        {14, 21, -14, -8, -16, 3},
        {21, 23, -14, -8, -16, 2},
        {23, 27, -13, -9, -16, 2},
        {27, 31, -13, -9, -16, 1},
        {31, 34, -13, -10, -16, 1},
        {34, 40, -12, -10, -16, 0},
        {40, 47, -12, -11, -16, -1},
        {47, 49, -11, -11, -16, -1},
        {49, 55, -11, -12, -16, -2},
        {55, 62, -10, -12, -16, -3},
        {62, 65, -10, -13, -16, -3},
        {65, 67, -10, -13, -16, -4},
        {67, 70, -9, -13, -16, -4},
        {70, 75, -9, -13, -15, -4},
        {75, 78, -9, -13, -15, -5},
        {78, 87, -8, -14, -15, -5},
        {87, 93, -8, -14, -15, -6},
        {93, 97, -7, -14, -15, -6},
        {97, 100, -7, -14, -15, -7},
        {100, 104, -7, -15, -15, -7},
        {104, 107, -7, -15, -14, -7},
        {107, 111, -6, -15, -14, -7},
        {111, 119, -6, -15, -14, -8},
        {119, 133, -5, -15, -14, -8},
        {133, 138, -5, -15, -13, -9},
        {138, 148, -4, -15, -13, -9},
        {148, 154, -4, -16, -13, -9},
        {154, 160, -4, -16, -13, -10},
        {160, 166, -3, -16, -13, -10},
        {166, 188, -3, -16, -12, -10},
        {188, 214, -2, -16, -12, -11},
        {214, 225, -1, -16, -11, -11},
        {225, 261, -1, -16, -11, -12},
        {261, 308, 0, -16, -10, -12},
        {308, 349, 1, -16, -10, -13},
        {349, 401, 1, -16, -9, -13},
        {401, 470, 2, -16, -9, -13},
        {470, 514, 2, -16, -8, -14},
        {514, 814, 3, -16, -8, -14},
        {814, 1143, 4, -16, -7, -14},
        {1143, 1908, 4, -15, -7, -15},
        {1908, 1435458304, 5, -15, -6, -15},
        {1435458304, 0, 5, -15, -5, -15},};
    
//    public static byte[] nameOfJadSizeProperty = {77,73,68,108,101,116,45,74,97,114,45,83,105,122,101};

    public static void drawLabel(Graphics g, String text, int x, int y, int textColor, int backColor) {
        int currentColor = g.getColor();
        Font font = g.getFont();
        int fontHeght = font.getHeight() + 4;
        int strLen = font.stringWidth(text) + 4;
        g.setColor(backColor);
        g.fillRect(x, y, strLen, fontHeght);
        g.setColor(textColor);
        g.drawString(text, x + 2, y + 2, Graphics.TOP | Graphics.LEFT);
        g.drawRect(x, y, strLen, fontHeght);
        g.setColor(currentColor);
    }

    public static void drawPointer(Graphics g, int x1, int y1, int x2, int y2, int color) {
        int currentColor = g.getColor();
        g.setColor(color);
        int sign, sign2, tan;
        int[] shifts;
        if (x2 == x1) {
            if (y2 > y1) {
                sign = 1;
            } else {
                sign = -1;
            }
            if ((x2 > x1) && (y2 < y1) || (x2 < x1) && (y2 > y1)) {
                sign2 = -1;
            } else {
                sign2 = 1;
            }
            shifts = ShiftsOfPointer[ShiftsOfPointer.length - 1];
            g.fillTriangle(x2, y2,
                    x2 + shifts[2] * sign * sign2,
                    y2 + shifts[3] * sign,
                    x2 + shifts[4] * sign * sign2,
                    y2 + shifts[5] * sign);
        } else {
            tan = Math.abs(100 * (y2 - y1) / (x2 - x1));
            if (y2 > y1) {
                sign = 1;
            } else {
                sign = -1;
            }
            if ((x2 > x1) && (y2 < y1) || (x2 < x1) && (y2 > y1)) {
                sign2 = -1;
            } else {
                sign2 = 1;
            }
            if (y2 == y1) {
                shifts = ShiftsOfPointer[0];
                if (x2 >= x1) {
                    sign = -sign;
                }
                g.fillTriangle(x2, y2,
                        x2 + shifts[2] * sign * sign2,
                        y2 + shifts[3] * sign,
                        x2 + shifts[4] * sign * sign2,
                        y2 + shifts[5] * sign);

            } else {
                for (int i = 0; i < ShiftsOfPointer.length; i++) {
                    shifts = ShiftsOfPointer[i];
                    if ((tan > shifts[0]) && ((tan <= shifts[1]) || (i == ShiftsOfPointer.length - 1))) {
                        g.fillTriangle(x2, y2,
                                x2 + shifts[2] * sign * sign2,
                                y2 + shifts[3] * sign,
                                x2 + shifts[4] * sign * sign2,
                                y2 + shifts[5] * sign);
                        break;
                    }
                }
            }
        }
        g.setColor(currentColor);

    }

    public static Image getMapImage(int col, int row) throws IOException {
        return Image.createImage(mapImagesFolder + mapImages[col][row] + ".png");
    }

    public static void drawTarget(Graphics g, int x, int y, int color) {
        int currentColor = g.getColor();
        g.setColor(color);
        g.setColor(0x00ffffff);
        g.fillRect(x - 3, y - 3, 6, 6);
        g.setColor(color);
        g.fillRect(x - 11, y - 1, 8, 3);
        g.fillRect(x + 3, y - 1, 8, 3);
        g.fillRect(x - 1, y - 11, 3, 8);
        g.fillRect(x - 1, y + 3, 3, 8);
        g.setColor(currentColor);

    }

    //Процедура рисования линни заданной толщины, со встроенной функцией sqrt()
    public void fillLineI(Graphics g, int x1, int y1, int x2, int y2, int w, int color) {
        int currentColor = g.getColor();
        g.setColor(color);
        if (w == 1) {
            g.drawLine(x1, y1, x2, y2);
        } else {
            if (y1 == y2) {
                if (x1 > x2) {
                    g.fillRect(x2, y1 - w / 2, 1 + x1 - x2, w);
                } else {
                    g.fillRect(x1, y2 - w / 2, 1 + x2 - x1, w);
                }
                return;
            }
            if (x1 == x2) {
                if (y1 < y2) {
                    g.fillRect(x1 - w / 2, y1, w, 1 + y2 - y1);
                } else {
                    g.fillRect(x1 - w / 2, y2, w, 1 + y1 - y2);
                }
                return;
            }
            int a, b, l, lx1, lx2, lx3, lx4, ly1, ly2, ly3, ly4;
            a = (x2 - x1);
            b = (y2 - y1);
            l = 100 * (b * b + a * a);
            lx1 = l;
            lx2 = l;
            if (l > 0 && w > 0) {
                while (true) {
                    lx1 = (lx2 / lx1 + lx1) / 2;
                    if (l > lx1) {
                        l = lx1;
                    } else {
                        break;
                    }
                }
                lx1 = (5 + b * 100 * w / 2 / l + x1 * 10) / 10;
                ly1 = (5 + y1 * 10 - a * 100 * w / 2 / l) / 10;
                lx2 = (5 + b * 100 * w / 2 / l + x2 * 10) / 10;
                ly2 = (5 + y2 * 10 - a * 100 * w / 2 / l) / 10;
                lx3 = (5 + x1 * 10 - b * 100 * (w - (w / 2) - 1) / l) / 10;
                ly3 = (5 + a * 100 * (w - (w / 2) - 1) / l + y1 * 10) / 10;
                lx4 = (5 + x2 * 10 - b * 100 * (w - (w / 2) - 1) / l) / 10;
                ly4 = (5 + a * 100 * (w - (w / 2) - 1) / l + y2 * 10) / 10;
                g.fillTriangle(lx1, ly1, lx3, ly3, lx2, ly2);
                g.fillTriangle(lx4, ly4, lx3, ly3, lx2, ly2);
                if (Math.abs(a) > Math.abs(b)) {
                    g.drawLine(lx1, ly1, lx2, ly2);
                    g.drawLine(lx3, ly3, lx4, ly4);
                    g.drawLine(lx4, ly4, lx2, ly2);
                    g.drawLine(lx1, ly1, lx3, ly3);
                }
            }
        }
        g.setColor(currentColor);
    }

    public void startMoving(final int keyCode) {
        isMoving = !isMoving;
        if (isMoving) {
            new Thread(new RunnableImpl(keyCode, this)).start();
        }
    }

    public void stopMoving() {
        isMoving = false;
    }

    abstract void moveLeft(final int STEP);

    abstract void moveRight(final int STEP);

    abstract void moveUp(final int STEP);

    abstract void moveDown(final int STEP);

    public void pointerPressed(int x, int y) {
        startPointX = x;
        startPointY = y;
    }

    public void pointerReleased(int x, int y) {
        int gDist = Math.abs(startPointX - x);
        int vDist = Math.abs(startPointY - y);

        int gSteps = gDist / STEP;
        int vSteps = vDist / STEP;

        if (startPointX > x) {
            for (int i = 0; i < gSteps; i++) {
                moveRight(STEP);
            }
            moveRight(gDist % STEP);
        } else {
            for (int i = 0; i < gSteps; i++) {
                moveLeft(STEP);
            }
            moveLeft(gDist % STEP);
        }
        if (startPointY > y) {
            for (int i = 0; i < vSteps; i++) {
                moveDown(STEP);
            }
            moveDown(vDist % STEP);
        } else {
            for (int i = 0; i < vSteps; i++) {
                moveUp(STEP);
            }
            moveUp(vDist % STEP);
        }
        if (this.touchCommands != null) {
            for (int i = 0; i < this.touchCommands.length; i++) {
                if (x == startPointX && y == startPointY && touchCommands[i].clicked(startPointX, startPointY)) {
                    touchCommands[i].click();
                }
            }
        }
        repaint();
    }

    public void pointerDragged(int x, int y) {
        pointerReleased(x, y);
        pointerPressed(x, y);
    }

    public void keyRepeat(int keyCode) {
        switch (keyCode) {
            case KEY_NUM1: {
                moveLeft(STEP);
                moveUp(STEP);
                repaint();
                break;
            }
            case KEY_NUM3: {
                moveRight(STEP);
                moveUp(STEP);
                repaint();
                break;
            }
            case KEY_NUM7: {
                moveLeft(STEP);
                moveDown(STEP);
                repaint();
                break;
            }
            case KEY_NUM9: {
                moveRight(STEP);
                moveDown(STEP);
                repaint();
                break;
            }
            case KEY_NUM4: {
                moveLeft(STEP);
                repaint();
                break;
            }
            case KEY_NUM2: {
                moveUp(STEP);
                repaint();
                break;
            }
            case KEY_NUM8: {
                moveDown(STEP);
                repaint();
                break;
            }
            case KEY_NUM6: {
                moveRight(STEP);
                repaint();
                break;
            }
            default: {
                keyCode = getGameAction(keyCode);
                switch (keyCode) {
                    case UP: {
                        moveUp(STEP);
                        repaint();
                        break;
                    }
                    case DOWN: {
                        moveDown(STEP);
                        repaint();
                        break;
                    }
                    case LEFT: {
                        moveLeft(STEP);
                        repaint();
                        break;
                    }
                    case RIGHT: {
                        moveRight(STEP);
                        repaint();
                        break;
                    }
                }
            }
        }
    }
}
