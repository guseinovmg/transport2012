
import java.io.*;

public class Comp {

    private static final short[][] routes = new short[350][];
    private static int topRes;
    public static final int RAD = 30;
    public static int MAX_FOUNDED_COUNT;
    public static boolean notSearchBuses;
    public static short[][] foundedRoutes;
    public static boolean[] reverseFoundedRoute;
    private InputStream is;
    private DataInputStream dis;

    public Comp() {
        is = getClass().getResourceAsStream("/bo");
        dis = new DataInputStream(is);
        short masilength = 0, i, j, number;
        for (i = 0; i < routes.length - 1; i++) {
            try {
                number = dis.readShort();
                masilength = dis.readShort();
            } catch (IOException ioe) {
                break;
            }
            routes[i] = new short[masilength += 3];
            routes[i][0] = number;
            for (j = 1; j < masilength - 2; j++) {
                try {
                    routes[i][j] = dis.readShort();
                } catch (IOException ioe) {
                    System.out.println("nepravilniy file");
                    break;
                }
                routes[i][masilength - 1] = routes[i][masilength - 3];
                routes[i][masilength - 2] = routes[i][masilength - 4];
            }
        }

        notSearchBuses = false;
        MAX_FOUNDED_COUNT = 50;
        foundedRoutes = new short[MAX_FOUNDED_COUNT + 1][];
        reverseFoundedRoute = new boolean[foundedRoutes.length];

    }

    public static int getTopRes() {
        return topRes;
    }

    public static int getFirstFoundedRouteNumber() {
        if (topRes == 0) {
            return 0;
        } else {
            return foundedRoutes[0][0];
        }
    }

    public final void findRoutes(int SX, int SY, int FX, int FY)//throws Exception
    {
//System.out.println("findRoutes");
        //#if RESTRICT_TYPE =="ONE_ROUTE"
        if (TRANSPORT2012.activated) {
            MAX_FOUNDED_COUNT = 50;
        } else {
            MAX_FOUNDED_COUNT = 1;
        }
        //#elif RESTRICT_TYPE == "ONLY_TROLLS_AND_BUS"
//#         if (TRANSPORT2012.activated) {
//#             notSearchBuses = false;
//#         } else {
//#             notSearchBuses = true;
//#         }
        //#endif
        short number = 0;
        short[] masi;
        int x1, x2, y1, y2;
        topRes = 0;

        int sax = SX - RAD * 2;
        int sbx = SX - RAD;
        int scx = SX + RAD;
        int sdx = SX + RAD * 2;

        int say = SY - RAD * 2;
        int sby = SY - RAD;
        int scy = SY + RAD;
        int sdy = SY + RAD * 2;

        int fax = FX - RAD * 2;
        int fbx = FX - RAD;
        int fcx = FX + RAD;
        int fdx = FX + RAD * 2;

        int fay = FY - RAD * 2;
        int fby = FY - RAD;
        int fcy = FY + RAD;
        int fdy = FY + RAD * 2;

        for (int i = 0; (i < routes.length) && (topRes < 50); i++) {
            try {
                masi = routes[i];
                number = masi[0];
                if ((masi == null) || (topRes > 0 && foundedRoutes[topRes - 1][0] == number)) {
                    continue;
                }
            } catch (NullPointerException pe) {
                continue;
            }

            for (int j = 1; j < masi.length - 3; j += 2) {
                x1 = masi[j];
                y1 = masi[j + 1];
                x2 = masi[j + 2];
                y2 = masi[j + 3];
                if ((x1 < sdx && x1 > sax || y1 < sdy && y1 > say)
                        && ((x1 < scx && x1 > sbx && y1 < scy && y1 > sby)
                        || (x1 < scx && x1 > sax && y1 > scy && y2 < scy && x2 < sdx && x2 > sbx)
                        || (x1 < sbx && y1 > sby && y1 < sdy && x2 > sbx && y2 > say && y2 < scy)
                        || (x1 < sbx && y1 > say && y1 < scy && x2 > sbx && y2 > sby && y2 < sdy)
                        || (x1 < scx && x1 > sax && y1 < sby && y2 > sby && x2 > sbx && x2 < sdx)
                        || (x1 < sdx && x1 > sbx && y1 < sby && y2 > sby && x2 < scx && x2 > sax)
                        || (x1 > scx && y1 > say && y1 < scy && x2 < scx && y2 < sdy && y2 > sby)
                        || (x1 > scx && y1 > sby && y1 < sdy && x2 < scx && y2 > say && y2 < scy)
                        || (x1 > sbx && x1 < sdx && y1 > scy && x2 > sax && x2 < scx && y2 < scy))) {
                    for (int k = number < 0 ? 1 : j; k < masi.length - 3; k += 2) {
                        x1 = masi[k];
                        y1 = masi[k + 1];
                        x2 = masi[k + 2];
                        y2 = masi[k + 3];
                        if ((x1 < fdx && x1 > fax || y1 < fdy && y1 > fay)
                                && ((x1 < fcx && x1 > fbx && y1 < fcy && y1 > fby)
                                || (x1 < fcx && x1 > fax && y1 > fcy && y2 < fcy && x2 < fdx && x2 > fbx)
                                || (x1 < fbx && y1 > fby && y1 < fdy && x2 > fbx && y2 > fay && y2 < fcy)
                                || (x1 < fbx && y1 > fay && y1 < fcy && x2 > fbx && y2 > fby && y2 < fdy)
                                || (x1 < fcx && x1 > fax && y1 < fby && y2 > fby && x2 > fbx && x2 < fdx)
                                || (x1 < fdx && x1 > fbx && y1 < fby && y2 > fby && x2 < fcx && x2 > fax)
                                || (x1 > fcx && y1 > fay && y1 < fcy && x2 < fcx && y2 < fdy && y2 > fby)
                                || (x1 > fcx && y1 > fby && y1 < fdy && x2 < fcx && y2 > fay && y2 < fcy)
                                || (x1 > fbx && x1 < fdx && y1 > fcy && x2 > fax && x2 < fcx && y2 < fcy))) {
                            foundedRoutes[topRes] = masi;
                            reverseFoundedRoute[topRes] = k < j;
                            if (!((number < -99 || number > 99) && notSearchBuses)) {
                                TRANSPORT2012.result.append(number);
                            }
                            topRes++;
                            break;
                        }
                    }
                    break;
                }
            }
            TRANSPORT2012.result.setTitle(Strs.FOUNDED + " (" + topRes + ")");
        }
    }
}
