
import javax.microedition.lcdui.*;

public class CommandOnCanvas {

    public Command relatedCommand;
    public Displayable display;
    private int x, y, width, height, right, bottom, textColor, backColor;

    public int getBackColor() {
        return backColor;
    }

    public void setBackColor(int backColor) {
        this.backColor = backColor;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
        bottom = y + height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
        right = x + width;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
        right = x + width;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
        bottom = y + height;
    }
    public String text;

    public CommandOnCanvas(Command cmd, Displayable d, String text, int textColor, int backColor) {
        this.relatedCommand = cmd;
        this.display = d;
        this.text = text;
        this.backColor = backColor;
        this.textColor = textColor;
    }

    public void draw(Graphics g) {
        Map.drawLabel(g, text, x, y, textColor, backColor);
    }

    public boolean clicked(int x, int y) {
        return x > this.x && x < this.right && y < this.bottom && y > this.y;
    }

    public void click() {
        TRANSPORT2012.midlet.commandAction(relatedCommand, display);
    }
}
