
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.ImageItem;

public class AskToActivateForm extends Form {

    public Command searchCommand, watchRouteCommand, activateBySMSCommand;

    public AskToActivateForm() {
        super(Strs.FOUNDED);
        searchCommand = new Command(Strs.POISK, Command.ITEM, 3);
        watchRouteCommand = new Command(Strs.WATCHING, Command.ITEM, 1);
        activateBySMSCommand = new Command(Strs.KUPIT, Command.ITEM, 2);
        this.addCommand(searchCommand);
        this.addCommand(watchRouteCommand);
        this.addCommand(activateBySMSCommand);
        this.setCommandListener(TRANSPORT2012.midlet);
    }

    public void setInfo(int routeNumber, int allFoundedRoutesCount) {
        deleteAll();
        append(new ImageItem("", TRANSPORT2012.result.getRouteIcon(routeNumber), ImageItem.LAYOUT_2, TRANSPORT2012.result.getTransportType(routeNumber)));
        append("  " + Math.abs(routeNumber) + " " + TRANSPORT2012.result.getTransportType(routeNumber));
        append(Strs.ASK_FOR_ACTIVATION_ON_RESULT);
        setTitle(Strs.FOUNDED + " (" + allFoundedRoutesCount + ")");
    }
}
