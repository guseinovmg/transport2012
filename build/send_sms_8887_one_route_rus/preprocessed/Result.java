
import javax.microedition.lcdui.*;
import java.io.*;

public class Result extends List {

    public Command watchRouteCommand;
    public Command searchCommand;
    public Command exit;
    public Image busik;
    public Image autobus;
    public Image trolleybus;

    public Result(String title, int listType) {
        super(title, listType);
        watchRouteCommand = new Command(Strs.WATCHING, Command.ITEM, 2);
        searchCommand = new Command(Strs.POISK, Command.SCREEN, 8);
        exit = new Command(Strs.QUIT, Command.EXIT, 10);
        this.addCommand(watchRouteCommand);
        this.addCommand(searchCommand);
        this.addCommand(exit);
        try {
            busik = Image.createImage("/busik.png");
            autobus = Image.createImage("/autobus.png");
            trolleybus = Image.createImage("/trolleybus.png");
        } catch (IOException ioe) {
        }
        this.setCommandListener(TRANSPORT2012.midlet);
    }

    public Image getRouteIcon(int routeNumber) {
        Image result;
        if (Math.abs(routeNumber) > 99) {
            result = busik;
        } else {
            switch (routeNumber) {
                case 8:
                case 9:
                case 10:
                case -11:
                case -14:
                case 16:
                case -17: {
                    result = trolleybus;
                    break;
                }
                default: {
                    result = autobus;
                    break;
                }
            }
        }
        return result;
    }

    public void append(int routeNumber) {
        this.append(" " + Math.abs(routeNumber), getRouteIcon(routeNumber));
    }

    public String getTransportType(int routeNumber) {
        final Image icon = getRouteIcon(routeNumber);
        if (icon == busik) {
            return Strs.MARSHRUTKA;
        }
        if (icon == autobus) {
            return Strs.AUTOBUS;
        }
        if (icon == trolleybus) {
            return Strs.TROLLEYBUS;
        }
        return "";
    }
}
