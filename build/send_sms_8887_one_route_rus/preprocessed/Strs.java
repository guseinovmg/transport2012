
public final class Strs {

    public static final String YES;
    public static final String NO;
    public static final String BACK;
    public static final String HELP;
    public static final String QUIT;
    public static final String SELECT;
    public static final String SHOW;
    public static final String WATCHING;
    public static final String HELP_TEXT;
    public static final String TRANSPORT;
    public static final String BISHKEK;
    public static final String FOUNDED;
    public static final String OTKUDA;
    public static final String KUDA;
    public static final String ROUTES_NOT_FOUND;
    public static final String CANCEL_SMS_SENDING;
    public static final String ACTIVATION_SUCCESS;
    public static final String ASK_FOR_ACTIVATION_ON_RESULT;
    public static final String SMS_ERROR_TRY_LATER;
    public static final String KUPIT;
    public static final String SEND;
    public static final String ACTIVATION;
    public static final String POISK;
    public static final String WAITE_SMS_IS_SENDING;
    public static final String AUTOBUS;
    public static final String TROLLEYBUS;
    public static final String MARSHRUTKA;
    public static final String[] translitStrings;
    public static final String alphabet;
    public static final String SERIAL;

    static {
//#if LANG == "TRANSLITE"
//#      boolean doTrans = true;   
//#else
     boolean doTrans = false;   
//#endif
        if (doTrans) {
            translitStrings = new String[]{"a", "b", "v", "g", "d", "e", "yo", "zh", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "c", "ch", "sh", "sch", "j", "i", "j", "e", "yu", "ya", "A", "B", "V", "G", "D", "E", "Yo", "Zh", "Z", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H", "C", "Ch", "Sh", "Sch", "J", "I", "J", "E", "Yu", "Ya"};
            alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
        } else {
            translitStrings = null;
            alphabet = "";
        }
        YES = ruToTranslite(doTrans, "Да");
        NO = ruToTranslite(doTrans, "Нет");
        BACK = ruToTranslite(doTrans, "Назад");
        HELP = ruToTranslite(doTrans, "Справка");
        QUIT = ruToTranslite(doTrans, "Выход");
        SELECT = ruToTranslite(doTrans, "Выбрать");
        SHOW = ruToTranslite(doTrans, "Показать");
        WATCHING = ruToTranslite(doTrans, "Просмотр");
        HELP_TEXT = ruToTranslite(doTrans, " Справочник содержит все маршруты автобусов, троллейбусов и маршрутных такси г. Бишкек на 2012г. \n   Выберите точки на карте откуда и куда Вам нужно проехать, затем Вы можете просмотреть найденные маршруты на карте. Перемещаться на карте можно нажимая на клавиши стрелки или на цифры, или перемещая карту пальцем, если у телефона сенсорный дисплей. \n www.map.on.kg \n ©  2008-2012.\n guseinovmg@gmail.com ");
        TRANSPORT = ruToTranslite(doTrans, "ТРАНСПОРТ");
        BISHKEK = ruToTranslite(doTrans, "г.Бишкек");
        FOUNDED = ruToTranslite(doTrans, "Найденные");
        OTKUDA = ruToTranslite(doTrans, "ОТКУДА");
        KUDA = ruToTranslite(doTrans, "КУДА");
        CANCEL_SMS_SENDING = ruToTranslite(doTrans, "Отправление SMS отменено");
        ACTIVATION_SUCCESS = ruToTranslite(doTrans, " Поздравляем! \n Справочник маршрутов теперь будет работать без ограничений");
        ROUTES_NOT_FOUND = ruToTranslite(doTrans, " Маршруты по этому направлению не найдены");
        SMS_ERROR_TRY_LATER = ruToTranslite(doTrans, "При отправке SMS возникла ошибка, возможно не хватает денег на балансе или Вы находитесь вне зоны доступа сети, попробуйте позже");
        KUPIT = ruToTranslite(doTrans, "Купить");
        SEND = ruToTranslite(doTrans, "Отправить");
        ACTIVATION = ruToTranslite(doTrans, "Активация");
        POISK = ruToTranslite(doTrans, "Поиск");
        WAITE_SMS_IS_SENDING = ruToTranslite(doTrans, "Подождите, отправляется  SMS");
        AUTOBUS = ruToTranslite(doTrans, "автобус");
        TROLLEYBUS = ruToTranslite(doTrans, "троллейбус");
        MARSHRUTKA = ruToTranslite(doTrans, "маршрутка");
        SERIAL = "Serial";


        //#if RESTRICT_TYPE == "ONE_ROUTE"
        //#if ACTIVATION_TYPE == "SEND_SMS_BY_PHONE"
        ASK_FOR_ACTIVATION_ON_RESULT = ruToTranslite(doTrans, "\n\n   Внимание!\nСправочник маршрутов работает в ограниченном режиме, и показывает только 1-ый найденный маршрут. Чтобы просматривать все найденные маршруты на карте необходимо купить справочник. Выберите команду \"Купить\" в меню, и разрешите отправку SMS.\n Стоимость :\nдля абонентов Megacom -100 сом,\nО -100 сом,\nBeeline - $2 (по текущему курсу). \n \"поиск\" - начать новый поиск.");
        //#elif ACTIVATION_TYPE == "LOAD_FULL_VERSION"
//#         ASK_FOR_ACTIVATION =ruToTranslite(doTrans,"Это ограниченная версия, только первый найденный маршрут показан, отправте SMS на номер " + TRANSPORT2012.SHORT_PHONE_NUMBER + " с кодом " + TRANSPORT2012.PARTNER_CODE;
        //#endif
        //#elif RESTRICT_TYPE == "ONLY_TROLLS_AND_BUS"
        //#if ACTIVATION_TYPE == "SEND_SMS_BY_PHONE"
//#         ASK_FOR_ACTIVATION =ruToTranslite(doTrans,"Это ограниченная версия,  только автобусы и троллейбусы найти можно, нажмите Да чтобы отправить SMS ");;
        //#elif ACTIVATION_TYPE == "LOAD_FULL_VERSION"
//#         ASK_FOR_ACTIVATION =ruToTranslite(doTrans,"Это ограниченная версия, только автобусы и троллейбусы найти можно, отправте SMS на номер " + TRANSPORT2012.SHORT_PHONE_NUMBER + " с кодом " + TRANSPORT2012.PARTNER_CODE;
        //#endif
        //#elif RESTRICT_TYPE == "NONE"
//#         ASK_FOR_ACTIVATION =ruToTranslite(doTrans,"");
        //#endif

    }

    public static String ruToTranslite(boolean doTranslit, final String ruString) {
        if (doTranslit) {
            StringBuffer stBuf = new StringBuffer(ruString.length() + 12);
            char[] chars = ruString.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                int index = alphabet.indexOf(chars[i]);
                if (index == -1) {
                    stBuf.append(chars[i]);
                } else {
                    stBuf.append(translitStrings[index]);
                }
            }
//            System.out.println(stBuf.toString());
            return stBuf.toString();
        } else {
            return ruString;
        }

    }
}
