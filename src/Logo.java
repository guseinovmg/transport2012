
import javax.microedition.lcdui.*;

/**
 * Заставка которая показывается пользователю при запуске программы
 *
 */
public class Logo extends Canvas {

    public static Image logo;

    public void paint(Graphics g) {
        g.setColor(255, 255, 255);
        g.fillRect(0, 0, g.getClipWidth(), g.getClipHeight());
        if (logo != null) {
            g.drawImage(logo, g.getClipWidth() / 2, g.getClipHeight() / 2, Graphics.VCENTER | Graphics.HCENTER);
        } else {
            g.setFont(Font.getFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_MEDIUM));
            g.drawString("TRANSPORT 2012", g.getClipWidth() / 2, g.getClipHeight() / 2, Graphics.VCENTER | Graphics.HCENTER);
        }
        logo = null;
    }

    public Logo() {
        try {
            logo = Image.createImage(getLogoFilePath(Math.max(this.getWidth(), this.getHeight())));
        } catch (Exception e) {
        }
    }

    /**
     *
     * @param _maxWidth ширина дисплея
     * @return имя файла, подходящего по размеру для данной ширины дисплея
     */
    public static String getLogoFilePath(int maxSize) {
        if (maxSize < 190) {
            return "/Logo120.png";
        } else {
            return "/Logo190.png";
        }
    }
}
